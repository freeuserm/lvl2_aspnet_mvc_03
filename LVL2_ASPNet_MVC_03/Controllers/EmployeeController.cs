﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        EmployeeContext db = new EmployeeContext();

        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    //if (ModelState.IsValid)
                    //{
                        var removeRecord = db.Employees.Where(x => x.Id == 1).FirstOrDefault();
                        db.Employees.Remove(removeRecord);
                        db.Employees.Add(employee);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                        
                    //}
                    
                }
                catch(Exception x)
                {
                    transaction.Rollback();
                    return RedirectToAction("Index");
                }
            }
           // return View(employee);
        }

        public ActionResult Details(int id)
        {
            return View(db.Employees.Where(x => x.Id == id).FirstOrDefault());
        }

        public ActionResult Edit(int id)
        {
            return View(db.Employees.Where(x => x.Id == id).FirstOrDefault());
        }
        [HttpPost]
        public ActionResult Edit(int id, Employee employee)
        {
            db.Entry(employee).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
            
        }

        public ActionResult Delete(int id)
        {
            return View(db.Employees.Where(x => x.Id == id).FirstOrDefault());
        }
        [HttpPost]
        public ActionResult Delete(int id, Employee employee)
        {
            employee = db.Employees.Where(x => x.Id == id).FirstOrDefault();
            db.Employees.Remove(employee);
            db.SaveChanges();

            //db.Entry(employee).State = System.Data.Entity.EntityState.Deleted;
            //db.SaveChanges();

            return RedirectToAction("Index");
        }
}
    }
